const Mongoose = require('mongoose');

const schema = new Mongoose.Schema({
  firstName: { type: String, default: '', trim: true },
  lastName: { type: String, default: '', trim: true },
  sqlUserId: { type: String, require: true, trim: true },
})

module.exports = Mongoose.model('Users', schema);
