const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const schema = new Mongoose.Schema({
  name: { type: String, trim: true },
  type: { type: String, required: true, enum: ['G', 'P'] },
  members: [{ type: Schema.Types.ObjectId, ref: 'Users' }]
}, { timestamps: true });

module.exports = Mongoose.model('Conversations', schema);
