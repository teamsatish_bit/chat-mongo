const Mongoose = require('mongoose');
const Schema = Mongoose.Schema;

const schema = new Mongoose.Schema({
  conversationId: { type: Schema.Types.ObjectId, ref: 'Conversations' },
  body: { type: String, trim: true },
  author: { type: Schema.Types.ObjectId, ref: 'Users' },
  localUid: { type: String, trim: true },
  localUidDateTime: [{
    _id: false,
    date: { type: String },
    time: { type: String }
  }],
  delivery: [{
    _id: false,
    date: { type: String },
    time: { type: String }
  }],
  received: [{
    _id: false,
    date: { type: String },
    time: { type: String }
  }],
}, { timestamps: true })

schema.virtual('dateAndTime').get(function () {
  const dateTime = Date.now()
  let time = this.createdAt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
  let date = this.createdAt.toISOString().split('T')[0]
  return { date: date, time: time };
})
module.exports = Mongoose.model('Messages', schema);
