const Messages = require('../db/models/Message');

module.exports = (io, socket) => {
  socket.on('sendMessage', async ({ author, body, conversationId }) => {
    const createMessage = await Messages.create({ author, body, conversationId });
    io.emit(conversationId, createMessage);
  });

  // Handler for 'received' event
  socket.on('received', function ({recieverId, messageId}) {
    var options = {
      timetoken: Date.now(),
      recieverId: recieverId,
      messageId: messageId
    };

    // TODO: update Message model where messageId=messageId
    // Emit 'delivered' event
    socket.emit('delivered', options);
  });

  // Handler for 'markSeen' event
  socket.on('markSeen', function (options) {
    var options = {
      timetoken: Date.now(),
      recieverId: recieverId,
      messageId: messageId
    };

    // TODO: update Message model where messageId = messageId
    // Emit 'markedSeen' event
    socket.emit('markedSeen', options);
  });
};
