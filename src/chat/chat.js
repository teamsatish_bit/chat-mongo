const onSendMessage = require('./sendMessage');

module.exports = (io) => {
  io.on('connection', (socket) => {

    socket.broadcast.emit('hi');
    socket.emit('hi');

    socket.on('chat message', (msg) => {
      io.emit('chat message', msg);
    });


    onSendMessage(io, socket);
    console.log('a user connected');

    socket.on('disconnect', (args) => {
      console.log(`disconnected ${socket.id}`);
    });
  });
};
