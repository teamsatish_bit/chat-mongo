const express = require('express');
const router = express.Router();
const aeh = require('../../src/middleware/asyncErrorHandler');
const Conversation = require('../db/models/Conversation');
const Message = require('../db/models/Message');
const Mongoose = require('mongoose');

router.get('/', aeh(async function (req, res) {
  const allConversations = await Conversation.find().sort({ updatedAt: -1 });
  return res.success(null, allConversations);
}));

router.get('/users/:userId', aeh(async function (req, res) {
  const { userId } = req.params;
  const allConversations = await Conversation.find({ "members": userId }).sort({ updatedAt: -1 });
  return res.success(null, allConversations);
}));

router.get('/:id', aeh(async function (req, res) {
  const { id } = req.params;
  const conversation = await Message.find({ "conversationId": id }).populate('author').sort({ 'createdAt': -1 })
  return res.success("Message List", conversation);
}));

router.post('/', aeh(async function (req, res) {
  const allConversations = await Conversation.create(req.body);
  return res.success("created successfully", allConversations);
}));

router.post('/:id/add-group-member', aeh(async function (req, res) {
  const { id } = req.params;
  const { members } = req.body;
  const conversation = await Conversation.update({ _id: id }, { $addToSet: { members: members } });
  return res.success("Member Added successfylly", conversation);
}));

router.post('/:id/remove-group-member', aeh(async function (req, res) {
  const { id } = req.params;
  const { members } = req.body;

  const conversation = await Conversation.update({ _id: id }, { $pull: { members: members } });
  return res.success("Member Removed successfylly", conversation);
}));
module.exports = router;