const express = require('express');
const router = express.Router();
const User = require('../db/models/User');
const Message = require('../db/models/Message');
const Coversation = require('../db/models/Conversation');
const aeh = require('../../src/middleware/asyncErrorHandler');

router.get('/', aeh(async function (req, res) {
  const allUsers = await User.find();
  return res.success("User found successfully", allUsers)
}));

// TODO: Add req.body validation.
router.post('/', aeh(async function (req, res) {
  const data = req.body ? req.body : '';
  if (!data.sqlUserId) {
    return res.status(404).error("Please pass firstName, lastName and sqlUserId in body")
  }
  const allUsers = await User.create(data);
  return res.success("User Saved successfully", allUsers)
}));

//Get user by SQlId
router.get('/get-user-by-sql-id/:sqlUserId', aeh(async function (req, res) {
  const allUsers = await User.findOne({ sqlUserId: req.params.sqlUserId });
  return res.success("User found successfully", allUsers);
}));

// Save Message in db

router.post('/save-message', aeh(async function (req, res) {
  const message = await Message.create(req.body);
  res.success("Message Saved Successfully", message)
}));

//Get Sorted Messages By userId
router.get('/:userId/recent-chats', aeh(async function (req, res) {
  const { userId } = req.params;
  const allCoversation = await Coversation.find({ "members": userId })
  var message = []
  if (allCoversation && allCoversation.length > 0) {
    for (const x of allCoversation) {
      const currentConvId = x._id
      const currentConvType = x.type
      const latestMessageOfConversation = await Message.find({ conversationId: currentConvId }).sort({ createdAt: 1 }).limit(1)
      if (latestMessageOfConversation && latestMessageOfConversation.length > 0) {
        const currentUserId = latestMessageOfConversation[0].author
        const time = latestMessageOfConversation[0].createdAt.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
        const date = latestMessageOfConversation[0].createdAt.toISOString().split('T')[0]
        const currentUser = await User.find({ _id: currentUserId })
        message.push({
          conversationId: currentConvId,
          conversationType: currentConvType,
          msg: latestMessageOfConversation[0].body,
          author: currentUser,
          createdAt: latestMessageOfConversation[0].createdAt,
          time,
          date
        })
      }
    }
  }
  if (message && message.length > 0) {
    message.sort(function compare(a, b) {
      let dateA = new Date(a.createdAt);
      let dateB = new Date(b.createdAt);
      return dateB - dateA;
    });
    res.success("Chats found successfully", message);
  } else {
    return res.success("No chats found")
  }
}));

module.exports = router;